<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
  <link rel="stylesheet" media="all" href="style.css" type="text/css"/>
  <link rel="stylesheet" href="font-awesome-4.6.3/css/font-awesome.css">


  <title>Amélie DELORY - Site professionnel</title>
</head>
<body>



  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">

      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="navbar-brand">

          <a class="logo" href="#ancre_profile">AM&#201;LIE DELORY  </a>
          <a class="logo hidden-xs hidden-sm" href="https://www.linkedin.com/in/amélie-delory-9707b111b" target="_blank"><img src="images/in.png" alt="" /></a>
          <a class="logo hidden-xs hidden-sm" href="https://framagit.org/u/AmelieD" target="_blank"><img src="images/github.png" alt="" /></a>
          <a class="logo hidden-xs hidden-sm" href="images/cv.pdf" target="_blank"><img src="images/pdf.png" alt="" /></a>


        </div>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li><a href= "#ancre_skills">Compétences</a></li>
          <li><a href= "#ancre_experience">&#201;xpérience</a></li>
          <li><a href= "#ancre_education">Parcours</a></li>
          <!--<li><a href= "#ancre_skills">Projets</a></li>-->
          <li><a href= "#ancre_interests">Intérêts</a></li>
          <li><a href= "#ancre_contact">Contact</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="clavier">
    <div class="box1 bg ">
      <div class="container">
        <div class="row">
          <div class="col-md-7">



          <h5>  D&#201;VELOPPEUSE <span class="fat">&</span>  INTEGRATRICE
            <span class="fat">&omega;eb <span class="et">et</span> mobile</h5>

          </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container">

      <div class="row">
        <span id="ancre_profile"></span>

        <div class="col-md-6  col-xs-12">
          <div  id="profile" >
            <div class="titre">
              <h4> Profil</h4>
            </div>
            <div class="list">
              <ul>
                <li class="dark">20 mars 1992 (24 ans)<br>

                  <span class="light">Arras, FRANCE</span>
                  <li class="dark">E-mail<br>
                    <span class="middle"><a href="mailto:amelie.delory@hotmail.fr">amelie.delory@hotmail.fr</a></span><br>
                  </li>
                  <li class="dark">Permis B<br>
                    <span class="middle">Véhiculée</span><br>
                  </li>

                </ul>
              </div>
            </div>
          </div>
          <span id="ancre_skills"></span>
          <div class="col-md-6 col-xs-12 ">
            <div id="skills">
              <div class="titre">
                <h4>Compétences</h4>
              </div>
              <div class="list">
                <ul>
                  <li class="dark"> Langages<br>
                    <span class="light">HTML5, CSS3, Ruby, Python, PHP, MySql</span>
                  </li>
                  <li class="dark"> Framewoks<br>
                    <span class="light">Bootstrap, Materialize, JQuery, JavaScript, Ruby on Rails, AngularJS, Ionic, Symfony </span>
                  </li>
                  <li class="dark">Environnement<br>
                    <span class="light">Debian, VirtualBox, Apache2, Nginx, Git, PhpMyAdmin</span>
                  </li>
                  <li class="dark">Autre<br>
                    <span class="light">Anglais courant, Administration système</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>


        <div class="row">
          <span id="ancre_experience"></span>

          <div class="col-md-6 col-xs-12  ">
            <div id="experience">
              <div class="titre">
                <h4>&#201;xpérience </h4>
              </div>
              <div class="list">
                <ul>
                  <li class="dark">2015<br>
                    <span class="middle">Centre de tri postal, Arras (62)</span><br>
                    <span class="light">Agent de production en horaires de nuit</span>
                  </li>
                  <li class="dark">2014<br>
                    <span class="middle">Vendanges, région Rhônes-Alpes (69)</span><br>
                  </li>
                  <li class="dark">2013<br>
                    <span class="middle">Orthopdie Meneghetti, Beaurains (62)</span><br>
                    <span class="light">Remplaçante attitrée de la secrétaire pendant les congés <br>(actualisation du site web, accueil, élaboration des plaquettes de tarifs avec excel...)</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <span id="ancre_education"></span>

          <div class="col-md-6 col-xs-12 ">
            <div id="education">
              <div class="titre">
                <h4> Parcours </h4>
              </div>
              <div class=list>
                <ul>
                  <li class="dark">2016<br>
                    <span class="middle">Formation <a href="http://pop.eu.com/popschool/">POPSchool</a></span> <span class="light">- Valenciennes</span>
                  </li>
                  <li class="dark">2014<br>
                    <span class="middle">BTS Assistant de Manager</span> <span class="light">- Lycée Guy Mollet, Arras</span><br>
                    <span class="light"> Stage au service des Relations Internationales à la mairie d'Arras </span>

                  </li>
                  <li class="dark">2012<br>
                    <span class="middle">1ere année LEA Anglais-Chinois</span> <span class="light">- Université d'Artois</span>
                    <br>
                  </li>
                  <li class="dark">2010<br>
                    <span class="middle">BAC L - Option Anglais</span> <span class="light">- Lycée Gambetta, Arras</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <span id="ancre_interests"></span>

          <div class="col-md-6 col-xs-12 ">
            <div id="interests">
              <div class="titre">
                <h4>Intérêts</h4>
              </div>
              <div class="list">
                <ul>
                  <li class="dark">Musique<br>
                    <span class="light">Chanteuse, guitariste depuis l'âge de 10 ans.<br> Productrice en M.A.O (Musique Assistée par Ordinateur)
<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/250237120&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>                    </span><br>
                  </li>
                  <li class="dark">Sport<br>
                    <span class="light">Badminton en compétition depuis 2010. Intégration d'une équipe d'interclub au niveau régional.</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <span id="ancre_contact"></span>

          <div class="col-md-6 col-xs-12 ">
            <div id="contact">
              <div class="titre">
                <h4> Contact </h4>
              </div>
              <?php
              if(isset($_POST['mail_contact'])) {
                if (!empty($_POST['email']) && !empty($_POST['contenu']) && isset($_POST['objet']) && isset($_POST['nom']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                  $destinataire = "amelie.delory@hotmail.fr";
                  $contenu = htmlspecialchars($_POST['contenu']);
                  $nom = htmlspecialchars($_POST['nom']);
                  $email = htmlspecialchars($_POST['email']);
                  $subject =htmlspecialchars($_POST['objet']);
                  $message = "Nom : " . $nom
                  . "<br/>"
                  . "Sujet : " . $subject
                  . "<br/>"
                  . "Email : " . $email
                  . "<br/><br/>"
                  . "Message : <br/>"
                  . $contenu;
                  $headers = "From: " . $email ."\r\n";
                  $headers .= "Reply-To: ". $email ."\r\n";
                  $headers  .= 'MIME-Version: 1.0' . "\r\n";
                  $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                  mail($destinataire, "Formulaire de contact ", $message, $headers);
                  echo "<script type='text/javascript'>alert('L\'email a été correctement envoyé. Merci.')</script>";
                } else {
                  echo "<script type='text/javascript'>alert('Une erreur s\'est produite, veuillez vérifier les données saisies.')</script>";
                }
              }
              ?>
              <form method="POST" action="">
                <input class="form-control" type="text" name="nom" placeholder="Enter your name"/><br>
                <input  class="form-control" type="email" name="email"  placeholder="Enter your Email"/><br>
                <input class="form-control" type="text" name="objet"  placeholder="Enter your subject"/><br>
                <textarea class="form-control" name="contenu" rows="5" cols="58" placeholder="Enter your message"></textarea><br>
                <input class="form-control"  type="submit" name="mail_contact" value="Send message">
              </form>
            </div>
          </div>
        </div>
      </div>
      <hr class="hidden-md hidden-lg">
      <div class="container hidden-md hidden-lg">
        <div class="row">
          <div class="col-xs-4 text-center">
              <a class="logo" href="https://www.linkedin.com/in/amlie-delory-9707b111b" target="_blank"><img src="images/in.png" alt="" /></a>
          </div>
          <div class="col-xs-4 text-center">
              <a class="logo" href="https://framagit.org/u/AmelieD" target="_blank"><img src="images/github.png" alt="" /></a>
          </div>
          <div class="col-xs-4 text-center">
              <a class="logo" href="cv.pdf" target="_blank"><img src="images/pdf.png" alt="" /></a>
          </div>

        </div>

      </div>





      <!-- Latest compiled and minified JavaScript -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="bootstrap/js/bootstrap.js"></script>
    </body>
    </html>
